# Modelling-and-Prediction-of-Movies

In this project, a data set containing information about 650 movies is given and I have done some cleaning, exploratory data analysis to gain some interesting insights from the data and finally made a model to predict Rotten Tomatoes Rating of movies